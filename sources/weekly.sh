#!/usr/bin/env bash

DATA_DIR="$(dirname -- "$( readlink -f -- "$0"; )")/../data/"
DATE="$(date +%Y-%m-%d)"

cd "${DATA_DIR}"

git add .
git commit -m "Update ${DATE}"
git pull --rebase
git push
