#!/usr/bin/env bash

DATA_DIR="$(dirname -- "$( readlink -f -- "$0"; )")/../data/"
DATE=$(date +%Y-%m-%d)

USERS=$(mysql -B --skip-column-names -e "SELECT count(*) FROM gitea.user";)
REPOS=$(mysql -B --skip-column-names -e "SELECT count(*) FROM gitea.repository";)

USAGE_PERCENT=$(df -h --output=pcent /mnt/ceph-cluster | tail -n 1)
USAGE_GB=$(df -h --output=used --block-size G /mnt/ceph-cluster | tail -n 1)

if [[ ! -f "${DATA_DIR}/user-storage-growth.csv" ]]; then
	echo "date, users, repos, usage_percent, usage_GB
" > "${DATA_DIR}/history.csv"
fi

echo "${DATE}, ${USERS}, ${REPOS}, ${USAGE_PERCENT}, ${USAGE_GB}" >> "${DATA_DIR}/history.csv"
